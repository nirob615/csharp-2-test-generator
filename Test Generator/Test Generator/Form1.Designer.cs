﻿namespace Test_Generator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbChapterSelect = new System.Windows.Forms.ComboBox();
            this.btnDefaultTest = new System.Windows.Forms.Button();
            this.btnPreview = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkMultipleChoice = new System.Windows.Forms.CheckBox();
            this.chkOpenEnded = new System.Windows.Forms.CheckBox();
            this.chkTrueFalse = new System.Windows.Forms.CheckBox();
            this.btnCustomTest = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.printDialog1 = new System.Windows.Forms.PrintDialog();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbChapterSelect
            // 
            this.cmbChapterSelect.FormattingEnabled = true;
            this.cmbChapterSelect.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11"});
            this.cmbChapterSelect.Location = new System.Drawing.Point(16, 38);
            this.cmbChapterSelect.Name = "cmbChapterSelect";
            this.cmbChapterSelect.Size = new System.Drawing.Size(103, 21);
            this.cmbChapterSelect.TabIndex = 5;
            this.cmbChapterSelect.SelectedIndexChanged += new System.EventHandler(this.cmbChapterSelect_SelectedIndexChanged);
            // 
            // btnDefaultTest
            // 
            this.btnDefaultTest.Enabled = false;
            this.btnDefaultTest.Location = new System.Drawing.Point(12, 111);
            this.btnDefaultTest.Name = "btnDefaultTest";
            this.btnDefaultTest.Size = new System.Drawing.Size(274, 23);
            this.btnDefaultTest.TabIndex = 6;
            this.btnDefaultTest.Text = "Generate Default Test";
            this.btnDefaultTest.UseVisualStyleBackColor = true;
            this.btnDefaultTest.Click += new System.EventHandler(this.btnLoadQuestions_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Enabled = false;
            this.btnPreview.Location = new System.Drawing.Point(13, 169);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(81, 23);
            this.btnPreview.TabIndex = 9;
            this.btnPreview.Text = "Preview";
            this.btnPreview.UseVisualStyleBackColor = true;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Enabled = false;
            this.btnPrint.Location = new System.Drawing.Point(109, 169);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(81, 23);
            this.btnPrint.TabIndex = 10;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(205, 169);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(81, 23);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox1.Controls.Add(this.cmbChapterSelect);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(134, 93);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Select Chapter";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox2.Controls.Add(this.chkMultipleChoice);
            this.groupBox2.Controls.Add(this.chkOpenEnded);
            this.groupBox2.Controls.Add(this.chkTrueFalse);
            this.groupBox2.Enabled = false;
            this.groupBox2.Location = new System.Drawing.Point(152, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(134, 93);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Select Question Types";
            // 
            // chkMultipleChoice
            // 
            this.chkMultipleChoice.AutoSize = true;
            this.chkMultipleChoice.Location = new System.Drawing.Point(6, 42);
            this.chkMultipleChoice.Name = "chkMultipleChoice";
            this.chkMultipleChoice.Size = new System.Drawing.Size(107, 17);
            this.chkMultipleChoice.TabIndex = 14;
            this.chkMultipleChoice.Text = "> Multiple Choice";
            this.chkMultipleChoice.UseVisualStyleBackColor = true;
            this.chkMultipleChoice.CheckedChanged += new System.EventHandler(this.chkMultipleChoice_CheckedChanged);
            // 
            // chkOpenEnded
            // 
            this.chkOpenEnded.AutoSize = true;
            this.chkOpenEnded.Location = new System.Drawing.Point(6, 65);
            this.chkOpenEnded.Name = "chkOpenEnded";
            this.chkOpenEnded.Size = new System.Drawing.Size(95, 17);
            this.chkOpenEnded.TabIndex = 12;
            this.chkOpenEnded.Text = "> Open Ended";
            this.chkOpenEnded.UseVisualStyleBackColor = true;
            this.chkOpenEnded.CheckedChanged += new System.EventHandler(this.chkOpenEnded_CheckedChanged);
            // 
            // chkTrueFalse
            // 
            this.chkTrueFalse.AutoSize = true;
            this.chkTrueFalse.Location = new System.Drawing.Point(6, 19);
            this.chkTrueFalse.Name = "chkTrueFalse";
            this.chkTrueFalse.Size = new System.Drawing.Size(87, 17);
            this.chkTrueFalse.TabIndex = 13;
            this.chkTrueFalse.Text = "> True/False";
            this.chkTrueFalse.UseVisualStyleBackColor = true;
            this.chkTrueFalse.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // btnCustomTest
            // 
            this.btnCustomTest.Enabled = false;
            this.btnCustomTest.Location = new System.Drawing.Point(12, 140);
            this.btnCustomTest.Name = "btnCustomTest";
            this.btnCustomTest.Size = new System.Drawing.Size(274, 23);
            this.btnCustomTest.TabIndex = 17;
            this.btnCustomTest.Text = "Generate Custom Test";
            this.btnCustomTest.UseVisualStyleBackColor = true;
            this.btnCustomTest.Click += new System.EventHandler(this.btnCustomTest_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Text files|*.txt|All files|*.*";
            // 
            // printDialog1
            // 
            this.printDialog1.UseEXDialog = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(298, 204);
            this.Controls.Add(this.btnCustomTest);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.btnDefaultTest);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test Generator";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbChapterSelect;
        private System.Windows.Forms.Button btnDefaultTest;
        private System.Windows.Forms.Button btnPreview;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox chkMultipleChoice;
        private System.Windows.Forms.CheckBox chkOpenEnded;
        private System.Windows.Forms.CheckBox chkTrueFalse;
        private System.Windows.Forms.Button btnCustomTest;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.PrintDialog printDialog1;
    }
}

