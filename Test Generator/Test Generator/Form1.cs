﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Test_Generator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //please see the tfQuestion class for comments explaining how it, mcQuestion, and oeQuestion work. they all work
        //very similarly

        consolidatedQuestions allMCquestions = new consolidatedQuestions();
        consolidatedQuestions allTFquestions = new consolidatedQuestions();
        consolidatedQuestions allOEquestions = new consolidatedQuestions();

        public PreviewForm preview = new PreviewForm();
        List<string> test = new List<string>();
        string testItem;
        private void btnPreview_Click(object sender, EventArgs e)
        {
            preview.previewListBox.Items.Clear();
            if (allMCquestions.consolidatedmcQuestions[0, 1] != null)
            {
                preview.previewListBox.Items.Add("Multiple Choice Questions.");
                preview.previewListBox.Items.Add("Please circle the correct answer.");
                preview.previewListBox.Items.Add("");
                test.Add("Multiple Choice Questions.");
                test.Add("Please circle the correct answer.");
                test.Add("");
                for (int i = 0; i < allMCquestions.consolidatedmcQuestions.GetLongLength(0); i++)
                {
                    for (int j = 0; j < 7; j++)
                    {
                        if (j == 0)
                        {
                            testItem ="" + (i + 1);
                            test.Add(testItem);
                            preview.previewListBox.Items.Add(i + 1);
                        }
                        else if (j == 1 || j == 2 || j == 3 || j == 4 || j == 5)
                        {
                            test.Add(allMCquestions.consolidatedmcQuestions[i, j]);
                            preview.previewListBox.Items.Add(allMCquestions.consolidatedmcQuestions[i, j]);
                        }
                        else
                        {
                            test.Add(allMCquestions.consolidatedmcQuestions[i, j]);
                            preview.previewListBox.Items.Add(allMCquestions.consolidatedmcQuestions[i, j]);
                            preview.previewListBox.Items.Add("");
                            test.Add("");
                        }

                    }
                }
            }

            if (allTFquestions.consolidatedtfQuestions[0, 1] != null)
            {
                preview.previewListBox.Items.Add("");
                preview.previewListBox.Items.Add("True/False Questions.");
                preview.previewListBox.Items.Add("Please circle true or false.");
                preview.previewListBox.Items.Add("");
                test.Add("");
                test.Add("True/False Questions.");
                test.Add("Please circle true or false.");
                test.Add("");
                for (int i = 0; i < allTFquestions.consolidatedtfQuestions.GetLongLength(0); i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (j == 0)
                        {
                            testItem = "" + (1 + 1);
                            test.Add(testItem);
                            preview.previewListBox.Items.Add(i + 1);
                        }
                        else if (j == 1 || j == 2)
                        {
                            test.Add(allTFquestions.consolidatedtfQuestions[i, j]);
                            preview.previewListBox.Items.Add(allTFquestions.consolidatedtfQuestions[i, j]);
                        }
                        else
                        {
                            test.Add(allTFquestions.consolidatedtfQuestions[i, j]);
                            preview.previewListBox.Items.Add(allTFquestions.consolidatedtfQuestions[i, j]);
                            preview.previewListBox.Items.Add("");
                            test.Add("");
                        }
                    }
                }
            }

            if (allOEquestions.consolidatedoeQuestions[0, 1] != null)
            {
                preview.previewListBox.Items.Add("");
                preview.previewListBox.Items.Add("Open Ended Questions.");
                preview.previewListBox.Items.Add("Please answer these questions in a short essay format.");
                preview.previewListBox.Items.Add("");
                test.Add("");
                test.Add("Open Ended Questions.");
                test.Add("Please answer these questions in a short essay format.");
                test.Add("");
                for (int i = 0; i < allOEquestions.consolidatedoeQuestions.GetLongLength(0); i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        if (j == 0)
                        {
                            testItem = "" + (i + 1);
                            preview.previewListBox.Items.Add(i + 1);
                        }
                        else if (j == 1 || j == 2)
                        {
                            test.Add(allOEquestions.consolidatedoeQuestions[i, j]);
                            preview.previewListBox.Items.Add(allOEquestions.consolidatedoeQuestions[i, j]);
                        }
                        else
                        {
                            test.Add(allOEquestions.consolidatedoeQuestions[i, j]);
                            preview.previewListBox.Items.Add(allOEquestions.consolidatedoeQuestions[i, j]);
                            preview.previewListBox.Items.Add("");
                            test.Add("");
                        }
                    }
                }
            }
            
            preview.Show();
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                using (StreamWriter testFile = new StreamWriter(saveFileDialog1.FileName))
                {
                    foreach (var item in test)
                    {
                        testFile.WriteLine(item);
                    }
                }
            }
            
                
            //testFile.Close();
            MessageBox.Show("test file saved as TestFile.txt");
        }

        private void btnLoadQuestions_Click(object sender, EventArgs e)
        {
            btnPreview.Enabled = true;
            btnPrint.Enabled = true;
            btnSave.Enabled = true;
        }

        private void cmbChapterSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            groupBox2.Enabled = true;
            btnDefaultTest.Enabled = true;
        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            btnCustomTest.Enabled = true;
        }

        private void btnCustomTest_Click(object sender, EventArgs e)
        {
            btnPreview.Enabled = true;
            btnPrint.Enabled = true;
            btnSave.Enabled = true;
            

            

            int chapterPrompt = Convert.ToInt32(cmbChapterSelect.Text);

            if (chkMultipleChoice.Checked)
            {
                
                switch (chapterPrompt)
                {
                    /*
                    this applies for all three switch statements below
                    the switch statement seperates the class objects based on chapter so that way they don't overwrite each other.
                    */
                    case 1:
                        mcQuestion chap1mcQuestions = new mcQuestion();
                        chap1mcQuestions.readInMCQuestios(chapterPrompt);
                        chap1mcQuestions.MCGendQuestions = chap1mcQuestions.QPicker(chapterPrompt);
                        allMCquestions.addMCQuestions(chap1mcQuestions.MCGendQuestions);
                        break;
                    case 2:
                        mcQuestion chap2mcQuestions = new mcQuestion();
                        chap2mcQuestions.readInMCQuestios(chapterPrompt);
                        chap2mcQuestions.MCGendQuestions = chap2mcQuestions.QPicker(chapterPrompt);
                        allMCquestions.addMCQuestions(chap2mcQuestions.MCGendQuestions);
                        break;
                    case 3:
                        mcQuestion chap3mcQuestions = new mcQuestion();
                        chap3mcQuestions.readInMCQuestios(chapterPrompt);
                        chap3mcQuestions.MCGendQuestions = chap3mcQuestions.QPicker(chapterPrompt);
                        allMCquestions.addMCQuestions(chap3mcQuestions.MCGendQuestions);
                        break;
                    case 4:
                        mcQuestion chap4mcQuestions = new mcQuestion();
                        chap4mcQuestions.readInMCQuestios(chapterPrompt);
                        chap4mcQuestions.MCGendQuestions = chap4mcQuestions.QPicker(chapterPrompt);
                        allMCquestions.addMCQuestions(chap4mcQuestions.MCGendQuestions);
                        break;
                }
        }

        if (chkOpenEnded.Checked)
        {

                switch (chapterPrompt)
                {
                    case 1:
                        oeQuestion chap1oeQuestions = new oeQuestion();
                        chap1oeQuestions.readInOEQuestios(chapterPrompt);
                        chap1oeQuestions.OEGendQuestions = chap1oeQuestions.QPicker(chapterPrompt);
                        allOEquestions.addOEQuestions(chap1oeQuestions.OEGendQuestions);
                        break;
                    case 2:
                        oeQuestion chap2oeQuestions = new oeQuestion();
                        chap2oeQuestions.readInOEQuestios(chapterPrompt);
                        chap2oeQuestions.OEGendQuestions = chap2oeQuestions.QPicker(chapterPrompt);
                        allOEquestions.addOEQuestions(chap2oeQuestions.OEGendQuestions);
                        break;
                    case 3:
                        oeQuestion chap3oeQuestions = new oeQuestion();
                        chap3oeQuestions.readInOEQuestios(chapterPrompt);
                        chap3oeQuestions.OEGendQuestions = chap3oeQuestions.QPicker(chapterPrompt);
                        allOEquestions.addOEQuestions(chap3oeQuestions.OEGendQuestions);
                        break;
                    case 4:
                        oeQuestion chap4oeQuestions = new oeQuestion();
                        chap4oeQuestions.readInOEQuestios(chapterPrompt);
                        chap4oeQuestions.OEGendQuestions = chap4oeQuestions.QPicker(chapterPrompt);
                        allOEquestions.addOEQuestions(chap4oeQuestions.OEGendQuestions);
                        break;
                }
        }

        if (chkTrueFalse.Checked)
        {
                switch (chapterPrompt)
                {
                    case 1:
                        tfQuestion chap1tfQuestions = new tfQuestion();
                        chap1tfQuestions.readInTFQuestios(chapterPrompt);
                        chap1tfQuestions.TFGendQuestions = chap1tfQuestions.QPicker(chapterPrompt);
                        allTFquestions.addTFQuestions(chap1tfQuestions.TFGendQuestions);
                        break;
                    case 2:
                        tfQuestion chap2tfQuestions = new tfQuestion();
                        chap2tfQuestions.readInTFQuestios(chapterPrompt);
                        chap2tfQuestions.TFGendQuestions = chap2tfQuestions.QPicker(chapterPrompt);
                        allTFquestions.addTFQuestions(chap2tfQuestions.TFGendQuestions);
                        break;
                    case 3:
                        tfQuestion chap3tfQuestions = new tfQuestion();
                        chap3tfQuestions.readInTFQuestios(chapterPrompt);
                        chap3tfQuestions.TFGendQuestions = chap3tfQuestions.QPicker(chapterPrompt);
                        allTFquestions.addTFQuestions(chap3tfQuestions.TFGendQuestions);
                        break;
                    case 4:
                        tfQuestion chap4tfQuestions = new tfQuestion();
                        chap4tfQuestions.readInTFQuestios(chapterPrompt);
                        chap4tfQuestions.TFGendQuestions = chap4tfQuestions.QPicker(chapterPrompt);
                        allTFquestions.addTFQuestions(chap4tfQuestions.TFGendQuestions);
                        break;
                }
        }
        //string questionType = cmbChapterSelect.Text;
        /*switch (questionType)
        {
            case "Open Ended":
                switch (chapterPrompt)
                {
                    case 1:
                        oeQuestion chap1oeQuestions = new oeQuestion();
                        chap1oeQuestions.readInOEQuestios(chapterPrompt);
                        break;
                    case 2:
                        oeQuestion chap2oeQuestions = new oeQuestion();
                        chap2oeQuestions.readInOEQuestios(chapterPrompt);
                        break;
                }
                break;
            case "True/False":
                switch (chapterPrompt)
                {
                    case 1:
                        tfQuestion chap1tfQuestions = new tfQuestion();
                        chap1tfQuestions.readInTFQuestios(chapterPrompt);
                        break;
                    case 2:
                        tfQuestion chap2tfQuestions = new tfQuestion();
                        chap2tfQuestions.readInTFQuestios(chapterPrompt);
                        break;
                }
                break;
            case "Multiple Choice":
                switch (chapterPrompt)
                {
                    /*
                    this applies for all three switch statements below
                    the switch statement seperates the class objects based on chapter so that way they don't overwrite each other.
                    /

                    case 1:
                        mcQuestion chap1mcQuestions = new mcQuestion();
                        chap1mcQuestions.readInMCQuestios(chapterPrompt);
                        mcgendQuestions = chap1mcQuestions.QPicker();
                        break;
                    case 2:
                        mcQuestion chap2tfQuestions = new mcQuestion();
                        chap2tfQuestions.readInMCQuestios(chapterPrompt);
                        break;
                }
                break;
        }*/
                }

        private void chkMultipleChoice_CheckedChanged(object sender, EventArgs e)
        {
            btnCustomTest.Enabled = true;
        }

        private void chkOpenEnded_CheckedChanged(object sender, EventArgs e)
        {
            btnCustomTest.Enabled = true;
        }

        
    }
}

