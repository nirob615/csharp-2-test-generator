﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
namespace Test_Generator
{
    class tfQuestion  //true/false question class. inherits from question class
    {
        private string[,] tfQuestions = new string[20, 4];
        private string[,] tfgendQuestions = new string[9, 4];

        public tfQuestion()
        { }

        public string[,] TFQuestions
        { get; set; }

        public string[,] TFGendQuestions
        { get; set; }

        public void readInTFQuestios(int chapterPrompt)
        {
            StreamReader tfQuestionsFile = File.OpenText("True-FalseQuestionsFile.txt");
            int lineCount; //line counter used in the for loop that skips over to a specific chapter
            string skippedData; //string that just holds the lines of data read from the file in the skipping process
            int questionCount = 0; //this is just like lineCount above but its used in the for loops that read in relevant data
            switch (chapterPrompt)
            {
                case 1:
                    /*
                    the switch statement simply points the method to the right set of instructions for each chapter.
                    each chapter gets its own case (a default case that'll be used for chapters not yet supported will be added at a
                    later date).
                    regarding the for loop below I will walk you through it.
                    questionCount is simply the question count it counts to however many questions for this paticular type of question are
                    in this chapter (in this case its twelve).
                    the nested for loop reads in each part of the question into the array in the order of 
                    question number
                    chapter number
                    question text
                    the mcQuestion class has an additional four parts for each choice of answer A-D. its placed here in the order
                    and the correct answer comes last.
                    the outer for loop just runs the inner one for each question there is in the chapter.
                    */
                    //line below correctly sizes the array for the number of questions in this chapter
                    tfQuestions = new string[12, 4];
                    for (questionCount = 0; questionCount < 12; questionCount++)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            tfQuestions[questionCount, i] = tfQuestionsFile.ReadLine();

                        }
                    }
                    break;
                case 2:
                    /*
                    the first for loop is to skip to the chapter 2 section of questions
                    the second one actually reads in chapter 2's questions into the array
                    for more details on the secod loop please refer to the comments in case 1
                    */
                    //line below correctly sizes the array for the number of questions in this chapter
                    tfQuestions = new string[20, 4];
                    for (lineCount = 0; lineCount < 49; lineCount++)
                    {
                        skippedData = tfQuestionsFile.ReadLine();
                    }
                    for (questionCount = 0; questionCount < 20; questionCount++)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            tfQuestions[questionCount, i] = tfQuestionsFile.ReadLine();

                        }
                    }
                    break;
                case 3:
                    tfQuestions = new string[10, 4];
                    for (lineCount = 0; lineCount < 130; lineCount++)
                    {
                        skippedData = tfQuestionsFile.ReadLine();
                    }
                    for (questionCount = 0; questionCount < 10; questionCount++)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            tfQuestions[questionCount, i] = tfQuestionsFile.ReadLine();

                        }
                    }
                    break;
                case 4:
                    tfQuestions = new string[10, 4];
                    for (lineCount = 0; lineCount < 171; lineCount++)
                    {
                        skippedData = tfQuestionsFile.ReadLine();
                    }
                    for (questionCount = 0; questionCount < 10; questionCount++)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            tfQuestions[questionCount, i] = tfQuestionsFile.ReadLine();

                        }
                    }
                    break;
            }
            tfQuestionsFile.Close();
        }
   
        public string[,] QPicker(int chapterPrompt)
        {
            string[,] Qlist = new string[9,4];
            Random rnd = new Random();
            bool check = false;
            int temp;
            List<int> questionNumbers = new List<int>();

            switch (chapterPrompt)
            {
                case 1:
                    for (int i = 0; i < 9; i++)
                    {
                        temp = rnd.Next(0, 11);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 11); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.tfQuestions[temp, 0];
                        Qlist[i, 1] = this.tfQuestions[temp, 1];
                        Qlist[i, 2] = this.tfQuestions[temp, 2];
                        Qlist[i, 3] = this.tfQuestions[temp, 3];
                    }
                    break;

                case 2:
                    for (int i = 0; i < 9; i++)
                    {
                        temp = rnd.Next(0, 19);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 19); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.tfQuestions[temp, 0];
                        Qlist[i, 1] = this.tfQuestions[temp, 1];
                        Qlist[i, 2] = this.tfQuestions[temp, 2];
                        Qlist[i, 3] = this.tfQuestions[temp, 3];
                    }
                    break;
                case 3:
                    for (int i = 0; i < 9; i++)
                    {
                        temp = rnd.Next(0, 9);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 9); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.tfQuestions[temp, 0];
                        Qlist[i, 1] = this.tfQuestions[temp, 1];
                        Qlist[i, 2] = this.tfQuestions[temp, 2];
                        Qlist[i, 3] = this.tfQuestions[temp, 3];
                    }
                    break;
                case 4:
                    for (int i = 0; i < 9; i++)
                    {
                        temp = rnd.Next(0, 9);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 9); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.tfQuestions[temp, 0];
                        Qlist[i, 1] = this.tfQuestions[temp, 1];
                        Qlist[i, 2] = this.tfQuestions[temp, 2];
                        Qlist[i, 3] = this.tfQuestions[temp, 3];
                    }
                    break;
            }
                    return Qlist;
        }
    }
}
