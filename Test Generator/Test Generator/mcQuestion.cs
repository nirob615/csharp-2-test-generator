﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Test_Generator
{
    class mcQuestion
    {
        private string[,] mcQuestions = new string[36, 8];
        private string[,] mcgendQuestions = new string[10, 8];

        public mcQuestion()
        { }

        public string[,] MCQuestions
        { get; set; }

        public string[,] MCGendQuestions
        { get; set; }

        public void readInMCQuestios(int chapterPrompt)
        {
            StreamReader mcQuestionsFile = File.OpenText("MultipleChoiceQuestions.txt");
            int questionCount = 0;
            int lineCount;
            string skippedData;
            switch (chapterPrompt)
            {
                /*
                for a walkthrough of how this switch statement and the for loops in each of its cases please see the comments
                on the switch statement in tfQuestion. the switch statement and for loops all do the same thing they just have their 
                numbers changed to match the corresponding files to each class.
                */
                case 1:
                    //line below correctly sizes the array for the number of questions in this chapter
                    mcQuestions = new string[36, 8];
                    for (questionCount = 0; questionCount < 16; questionCount++)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            mcQuestions[questionCount, i] = mcQuestionsFile.ReadLine();

                        }
                    }
                    break;
                case 2:
                    //line below correctly sizes the array for the number of questions in this chapter
                    mcQuestions = new string[31, 8];
                    for (lineCount = 0; lineCount < 290; lineCount++)
                    {
                        skippedData = mcQuestionsFile.ReadLine();
                    }
                    for (questionCount = 0; questionCount < 31; questionCount++)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            mcQuestions[questionCount, i] = mcQuestionsFile.ReadLine();

                        }
                    }
                    break;
                case 3:
                    //line below correctly sizes the array for the number of questions in this chapter
                    mcQuestions = new string[30, 8];
                    for (lineCount = 0; lineCount < 539; lineCount++)
                    {
                        skippedData = mcQuestionsFile.ReadLine();
                    }
                    for (questionCount = 0; questionCount < 30; questionCount++)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            mcQuestions[questionCount, i] = mcQuestionsFile.ReadLine();

                        }
                    }
                    break;
                case 4:
                    //line below correctly sizes the array for the number of questions in this chapter
                    mcQuestions = new string[20, 8];
                    for (lineCount = 0; lineCount < 780; lineCount++)
                    {
                        skippedData = mcQuestionsFile.ReadLine();
                    }
                    for (questionCount = 0; questionCount < 20; questionCount++)
                    {
                        for (int i = 0; i < 8; i++)
                        {
                            mcQuestions[questionCount, i] = mcQuestionsFile.ReadLine();

                        }
                    }
                    break;
            }
            
        }

        public string[,] QPicker(int chapterPrompt)
        {
            string[,] Qlist = new string[10, 8];
            Random rnd = new Random();
            bool check = false;
            int temp;
            List<int> questionNumbers = new List<int>();

            switch (chapterPrompt)
            {
                case 1:
                    for (int i = 0; i < 10; i++)
                    {
                        temp = rnd.Next(0, 35);
                        while(check == false)
                        {
                            if(questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 35); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.mcQuestions[temp, 0];
                        Qlist[i, 1] = this.mcQuestions[temp, 1];
                        Qlist[i, 2] = this.mcQuestions[temp, 2];
                        Qlist[i, 3] = this.mcQuestions[temp, 3];
                        Qlist[i, 4] = this.mcQuestions[temp, 4];
                        Qlist[i, 5] = this.mcQuestions[temp, 5];
                        Qlist[i, 6] = this.mcQuestions[temp, 6];
                        Qlist[i, 7] = this.mcQuestions[temp, 7];
                    }
                    break;

                case 2:
                    for (int i = 0; i < 10; i++)
                    {
                        temp = rnd.Next(0, 30);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 30); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.mcQuestions[temp, 0];
                        Qlist[i, 1] = this.mcQuestions[temp, 1];
                        Qlist[i, 2] = this.mcQuestions[temp, 2];
                        Qlist[i, 3] = this.mcQuestions[temp, 3];
                        Qlist[i, 4] = this.mcQuestions[temp, 4];
                        Qlist[i, 5] = this.mcQuestions[temp, 5];
                        Qlist[i, 6] = this.mcQuestions[temp, 6];
                        Qlist[i, 7] = this.mcQuestions[temp, 7];
                    }
                    break;
                case 3:
                    for (int i = 0; i < 10; i++)
                    {
                        temp = rnd.Next(0, 29);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 29); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.mcQuestions[temp, 0];
                        Qlist[i, 1] = this.mcQuestions[temp, 1];
                        Qlist[i, 2] = this.mcQuestions[temp, 2];
                        Qlist[i, 3] = this.mcQuestions[temp, 3];
                        Qlist[i, 4] = this.mcQuestions[temp, 4];
                        Qlist[i, 5] = this.mcQuestions[temp, 5];
                        Qlist[i, 6] = this.mcQuestions[temp, 6];
                        Qlist[i, 7] = this.mcQuestions[temp, 7];
                    }
                    break;
                case 4:
                    for (int i = 0; i < 10; i++)
                    {
                        temp = rnd.Next(0, 19);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 19); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.mcQuestions[temp, 0];
                        Qlist[i, 1] = this.mcQuestions[temp, 1];
                        Qlist[i, 2] = this.mcQuestions[temp, 2];
                        Qlist[i, 3] = this.mcQuestions[temp, 3];
                        Qlist[i, 4] = this.mcQuestions[temp, 4];
                        Qlist[i, 5] = this.mcQuestions[temp, 5];
                        Qlist[i, 6] = this.mcQuestions[temp, 6];
                        Qlist[i, 7] = this.mcQuestions[temp, 7];
                    }
                    break;
            }
                return Qlist;
        }


        /*has a string for each answer choice (has a totel of four answer choices). it also has a char for the correct answer
        private string answerA;
        private string answerB;
        private string answerC;
        private string answerD;
        private string mcCorrectAnswer;
        private Question mcHead;
        private Question mcTail;

        public mcQuestion()
        {

        }

        public string AnswerA
        {
            get { return answerA; }
            set { answerA = value; }
        }

        public string AnswerB
        {
            get { return answerB; }
            set { answerB = value; }
        }

        public string AnswerC
        {
            get { return answerC; }
            set { answerC = value; }
        }

        public string AnswerD
        {
            get { return answerD; }
            set { answerD = value; }
        }

        public string MCCorrectAnswer
        {
            get { return mcCorrectAnswer; }
            set { mcCorrectAnswer = value; }
        }

        public Question MCHead
        {
            get { return mcHead; }
            set { mcHead = value; }
        }

        public Question MCTail
        {
            get { return mcTail; }
            set { mcTail = value; }
        }
        */


    }
        }
