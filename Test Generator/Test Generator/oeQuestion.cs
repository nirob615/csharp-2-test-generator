﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Test_Generator
{
    class oeQuestion  //open ended question class. inherits from question class.
    {
        private string[,] oeQuestions = new string[20, 4];
        private string[,] oegendQuestions = new string[7, 4];

        public oeQuestion()
        { }

        public string[,] OEQuestions
        { get; set; }

        public string[,] OEGendQuestions
        { get; set; }

        public void readInOEQuestios(int chapterPrompt)
        {
            Stream q = new MemoryStream();
            StreamReader oeQuestionsFile = File.OpenText("OpenEndedQuestionsFile.txt");
            int lineCount;
            string skippedData;
            int questionCount = 0;
            switch (chapterPrompt)
            {
                /*
                this switch statement was copied over from the tfQuestion class.
                the numbers will need to be changed to reflect how the end of the chapters line up with line numbers
                for a walkthrough of how this switch statement and the for loops in each of its cases please see the comments
                on the switch statement in tfQuestion. the switch statement and for loops all do the same thing they just have their 
                numbers changed to match the corresponding files to each class.
                */
                case 1:
                    //line below correctly sizes the array for the number of questions in this chapter
                    oeQuestions = new string[16, 4];
                    for (questionCount = 0; questionCount < 16; questionCount++)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            oeQuestions[questionCount, i] = oeQuestionsFile.ReadLine();

                        }
                    }
                    break;
                case 2:
                    //line below correctly sizes the array for the number of questions in this chapter
                    oeQuestions = new string[20, 4];
                    for (lineCount = 0; lineCount < 65; lineCount++)
                    {
                        skippedData = oeQuestionsFile.ReadLine();
                    }
                    for (questionCount = 0; questionCount < 20; questionCount++)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            oeQuestions[questionCount, i] = oeQuestionsFile.ReadLine();

                        }
                    }
                    break;
                case 3:
                    //line below correctly sizes the array for the number of questions in this chapter
                    oeQuestions = new string[12, 4];
                    for (lineCount = 0; lineCount < 145; lineCount++)
                    {
                        skippedData = oeQuestionsFile.ReadLine();
                    }
                    for (questionCount = 0; questionCount < 12; questionCount++)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            oeQuestions[questionCount, i] = oeQuestionsFile.ReadLine();

                        }
                    }
                    break;
                case 4:
                    //line below correctly sizes the array for the number of questions in this chapter
                    oeQuestions = new string[10, 4];
                    for (lineCount = 0; lineCount < 193; lineCount++)
                    {
                        skippedData = oeQuestionsFile.ReadLine();
                    }
                    for (questionCount = 0; questionCount < 10; questionCount++)
                    {
                        for (int i = 0; i < 4; i++)
                        {
                            oeQuestions[questionCount, i] = oeQuestionsFile.ReadLine();

                        }
                    }
                    break;
            }
        }

        public string[,] QPicker(int chapterPrompt)
        {
            string[,] Qlist = new string[7, 4];
            Random rnd = new Random();
            bool check = false;
            int temp;
            List<int> questionNumbers = new List<int>();

            switch (chapterPrompt)
            {
                case 1:
                    for (int i = 0; i < 7; i++)
                    {
                        temp = rnd.Next(0, 11);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 11); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.oeQuestions[temp, 0];
                        Qlist[i, 1] = this.oeQuestions[temp, 1];
                        Qlist[i, 2] = this.oeQuestions[temp, 2];
                        Qlist[i, 3] = this.oeQuestions[temp, 3]; ;

                    }
                    break;
                case 2:
                    for (int i = 0; i < 7; i++)
                    {
                        temp = rnd.Next(0, 19);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 19); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.oeQuestions[temp, 0];
                        Qlist[i, 1] = this.oeQuestions[temp, 1];
                        Qlist[i, 2] = this.oeQuestions[temp, 2];
                        Qlist[i, 3] = this.oeQuestions[temp, 3]; ;

                    }
                    break;
                case 3:
                    for (int i = 0; i < 7; i++)
                    {
                        temp = rnd.Next(0, 11);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 11); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.oeQuestions[temp, 0];
                        Qlist[i, 1] = this.oeQuestions[temp, 1];
                        Qlist[i, 2] = this.oeQuestions[temp, 2];
                        Qlist[i, 3] = this.oeQuestions[temp, 3]; ;

                    }
                    break;
                case 4:
                    for (int i = 0; i < 7; i++)
                    {
                        temp = rnd.Next(0, 9);
                        while (check == false)
                        {
                            if (questionNumbers.Contains(temp))
                            { temp = rnd.Next(0, 9); }
                            else
                            { check = true; }
                        }
                        Qlist[i, 0] = this.oeQuestions[temp, 0];
                        Qlist[i, 1] = this.oeQuestions[temp, 1];
                        Qlist[i, 2] = this.oeQuestions[temp, 2];
                        Qlist[i, 3] = this.oeQuestions[temp, 3]; ;

                    }
                    break;
            }

                    return Qlist;
        }
    }
        
}
